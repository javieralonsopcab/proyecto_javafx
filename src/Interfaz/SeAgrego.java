package Interfaz;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class SeAgrego implements Initializable{
    
    @FXML
    private Label titulo;
    
    @FXML
    void cmdCerrar() {
        Stage stage = (Stage)this.titulo.getScene().getWindow();
        stage.close();
    }
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

}
