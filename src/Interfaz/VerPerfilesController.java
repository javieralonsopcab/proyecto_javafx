/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.TiposProfesionales.Profesional;
import Modelo.TiposProfesionales.TipoProfesion;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author javier
 */
public class VerPerfilesController implements Initializable {

    @FXML
    private ImageView imagenPerfil;

    @FXML
    private Label txtNombre;

    @FXML
    private Label txtApellido;

    @FXML
    private Label txtEdad;

    @FXML
    private Label txtDireccion;

    @FXML
    private Label txtTelefono;

    @FXML
    private Label txtProfesion;

    @FXML
    private Label txtTarjetaPRofesional;

    @FXML
    private ComboBox<TipoProfesion> comboPerfiles;

    private ArrayList<Profesional> profesionales;

    private ArrayList<Profesional> tiposDeProfesionales;

    private int index = 0;

    private Profesional seleccionado = null;

    @FXML
    void cmdAnterior() throws FileNotFoundException {
        if (this.index == 0) {

        } else {
            this.index--;
            this.asignarProfesional(this.tiposDeProfesionales.get(index));
        }
    }

    @FXML
    void cambios() throws FileNotFoundException {
        TipoProfesion selected = this.comboPerfiles.getSelectionModel().getSelectedItem();
        this.tiposDeProfesionales = this.obtenerProfesionales(selected);
        this.index = 0;
        this.asignarProfesional(this.tiposDeProfesionales.get(this.index));
    }

    public void asignarProfesional(Profesional p) throws FileNotFoundException {
        if(p!=null){
        this.comboPerfiles.getSelectionModel().select(p.getProfesion());
        this.txtNombre.setText(p.getNombre());
        this.txtApellido.setText(p.getApellidos());
        this.txtDireccion.setText(p.getDireccion());
        this.txtEdad.setText(String.valueOf(p.getEdad()));
        this.txtProfesion.setText(p.getProfesion().toString());
        this.txtTelefono.setText(String.valueOf(p.getNumeroTelefono()));
        this.txtTarjetaPRofesional.setText(String.valueOf(p.getTarjetaProfesional()));
        this.cargarImagen(p);
        this.seleccionado = p;
        }else{
            this.colocarNull();
        }
    }

    public ArrayList<Profesional> obtenerProfesionales(TipoProfesion tp) {
        ArrayList<Profesional> nuevo = new ArrayList<>();
        for (Profesional p : this.profesionales) {
            if (p.getProfesion().equals(tp)) {
                nuevo.add(p);
            }
        }
        return nuevo;
    }

    @FXML
    void cmdSiguiente() throws FileNotFoundException {

        if (this.index < this.tiposDeProfesionales.size()) {
            this.index++;
            this.asignarProfesional(this.tiposDeProfesionales.get(index));
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TipoProfesion[] p = TipoProfesion.values();
        for (TipoProfesion p1 : p) {
            this.comboPerfiles.getItems().add(p1);
        }
        this.comboPerfiles.getSelectionModel().select(0);
        this.cargarProfesionales();
        try {
            this.cargarPrimero();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VerPerfilesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargarImagen(Profesional p) throws FileNotFoundException {
        File fis = new File("src\\Imagenes\\titulo-" + p.getNombre() + ".jpg");
        if (fis.exists()) {
            FileInputStream f = new FileInputStream(fis.getAbsolutePath());
            Image image1 = new Image(f);
            this.imagenPerfil.setImage(image1);
        } else {
            this.imagenPerfil.setImage(null);
        }
    }

    public void cargarPrimero() throws FileNotFoundException {
        this.cargarProfesionales();
        Profesional p = this.profesionales.get(0);
        if(p!=null){
        this.comboPerfiles.getSelectionModel().select(p.getProfesion());
        this.txtNombre.setText(p.getNombre());
        this.txtApellido.setText(p.getApellidos());
        this.txtDireccion.setText(p.getDireccion());
        this.txtEdad.setText(String.valueOf(p.getEdad()));
        this.txtProfesion.setText(p.getProfesion().toString());
        this.txtTelefono.setText(String.valueOf(p.getNumeroTelefono()));
        this.txtTarjetaPRofesional.setText(String.valueOf(p.getTarjetaProfesional()));
        this.cargarImagen(p);
        this.seleccionado = p;
        }
        else{
            this.colocarNull();
        }
    }
    
    private void colocarNull() throws FileNotFoundException{
        this.comboPerfiles.getSelectionModel().select(null);
        this.txtNombre.setText(null);
        this.txtApellido.setText(null);
        this.txtDireccion.setText(null);
        this.txtEdad.setText(String.valueOf(null));
        this.txtProfesion.setText(null);
        this.txtTelefono.setText(null);
        this.txtTarjetaPRofesional.setText(null);
        this.cargarImagen(null);
        this.seleccionado = null;
    }

    public void cargarProfesionales() {
        this.profesionales = CRUD.CRUDProfesional.read();

    }

    @FXML
    void cmdEliminar() throws FileNotFoundException {
        if (CRUD.CRUDProfesional.eliminar(this.seleccionado)) {
            this.cargarPrimero();
            System.out.println("Se elimino");
            this.cargarProfesionales();
            this.asignarProfesional(this.profesionales.get(0));
        }
        else{
        System.err.println("No se pudo eliminar");
        }
    }

    @FXML
    void cmdVerTitulo() throws IOException {
        Image imagen = this.imagenPerfil.getImage();
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "src\\Interfaz\\Titulo.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

        // Create the Pane and all Details
        Pane root = (Pane) loader.load(fxmlStream);
        // Create the Scene
        Titulo t = loader.getController();
        t.inicializar(this.txtNombre.getText(), imagen);
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Titulo");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
    }

    @FXML
    void cmdDescargar() {

    }
}
