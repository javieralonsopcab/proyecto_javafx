/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author javier
 */
public class ControladorMenuPrincipal implements Initializable{
    @FXML
    private Text titulo;
    @FXML
    void cmdAgregarPerfil() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "src\\Interfaz\\AgregarPersona.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

        // Create the Pane and all Details
        Pane root = (Pane) loader.load(fxmlStream);
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Tu Profesional");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
        
    }

    @FXML
    void cmdVerProfesionales() throws FileNotFoundException, IOException {
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "src\\Interfaz\\VerPerfiles.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        
        // Create the Pane and all Details
        Pane root = (Pane) loader.load(fxmlStream);

        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Cargando");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
   
    }
    
    
}
