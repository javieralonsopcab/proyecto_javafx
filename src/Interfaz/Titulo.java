package Interfaz;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Titulo implements Initializable{

    @FXML
    private ImageView imagenPrincipal;

    @FXML
    private Label txtNombre;

    @FXML
    void cmdRegresar() {
        Stage stage = (Stage)this.txtNombre.getScene().getWindow();
        stage.close();
    }
    
    public void inicializar(String nombre, Image imagen){
        this.txtNombre.setText(nombre);
        this.imagenPrincipal.setImage(imagen);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
     
    }

}
