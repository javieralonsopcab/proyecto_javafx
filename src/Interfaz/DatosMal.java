package Interfaz;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class DatosMal implements Initializable{

    @FXML
    private Label txtTitulo;

    @FXML
    void cmdRegresar() {
        Stage stage = (Stage)this.txtTitulo.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

}

