package Interfaz;

import CRUD.CRUDProfesional;
import Modelo.TiposProfesionales.Profesional;
import Modelo.TiposProfesionales.TipoProfesion;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AgregarPersonaController implements Initializable {

    @FXML
    private TextField txtgetNombre;

    @FXML
    private TextField txtgetApellidos;

    @FXML
    private TextField txtgetEdad;

    @FXML
    private TextField txtgetCedula;

    @FXML
    private TextField txtgetTarjetaProfesional;

    @FXML
    private TextField txtgetTitulo;

    @FXML
    private TextField txtgetDireccion;

    @FXML
    private TextField txtgetNumerTelefonico;

    @FXML
    private ComboBox<TipoProfesion> comboProfesion;

    private File titulo = null;
    String ruta = "";
    @FXML
    void cmdAgregar() throws FileNotFoundException, IOException {
        String nombre = this.txtgetNombre.getText();
        String apellidos = this.txtgetApellidos.getText();
        int edad = Integer.parseInt(this.txtgetEdad.getText());
        long cedula = Long.parseLong(this.txtgetCedula.getText());
        long tarjetaProfesional = Long.parseLong(this.txtgetTarjetaProfesional.getText());
        String direccion = this.txtgetDireccion.getText();
        long numero = Long.parseLong(this.txtgetNumerTelefonico.getText());
        Profesional nuevo = new Profesional(nombre, apellidos, edad, cedula, tarjetaProfesional, this.titulo, direccion, numero);
        nuevo.setProfesion(this.comboProfesion.getSelectionModel().getSelectedItem());
        boolean seAgrego = CRUDProfesional.crearProfesional(nuevo);
        if(seAgrego){
            this.abrirSeAgrego();
        }
        else{
            this.abrirNoSeAgrego();
        }
    }
    
    private void abrirNoSeAgrego() throws FileNotFoundException, IOException{
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "src\\Interfaz\\DatosMal.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        
        // Create the Pane and all Details
        Pane root = (Pane) loader.load(fxmlStream);
        
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Se agregó");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
        
        Stage stageN = (Stage) this.txtgetApellidos.getScene().getWindow();
        stageN.close();
    }
    
    private void abrirSeAgrego() throws FileNotFoundException, IOException{
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "src\\Interfaz\\SeAgrego.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        
        // Create the Pane and all Details
        Pane root = (Pane) loader.load(fxmlStream);
        
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        // Set the Title to the Stage
        stage.setTitle("Se agregó");
        stage.resizableProperty().setValue(Boolean.FALSE);
        // Display the Stage
        stage.show();
        
        Stage stageN = (Stage) this.txtgetApellidos.getScene().getWindow();
        stageN.close();
    }

    @FXML
    void cmdBuscar() throws IOException {
        Stage stage = (Stage)this.comboProfesion.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
       
        File selected = fileChooser.showOpenDialog(stage);
        String ruta = selected.getAbsolutePath();
        this.txtgetTitulo.setText(ruta);
        this.titulo = selected;
    }

    @FXML
    void cmdCancelar() {
        Stage stage = (Stage) this.txtgetApellidos.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TipoProfesion valores[] = TipoProfesion.values();
        for (TipoProfesion valore : valores) {
            this.comboProfesion.getItems().add(valore);
        }
    }

}
