/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CRUD;

import Modelo.TiposProfesionales.Profesional;
import Modelo.TiposProfesionales.TipoProfesion;
import SQL.MysqlDb;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author javier
 */
public class CRUDProfesional {

    public static boolean crearProfesional(Profesional nuevo) {
        MysqlDb m = new MysqlDb();
        Connection conexion = m.getConnection();
        try {
            String sql = "INSERT INTO DatosPersonales VALUES(?,?,?,?,?,?,?,?,?)";
            PreparedStatement sentence = conexion.prepareStatement(sql);
            sentence.setString(1, nuevo.getNombre());
            sentence.setString(2, nuevo.getApellidos());
            sentence.setInt(3, nuevo.getEdad());
            sentence.setInt(4, (int) nuevo.getCedula());
            sentence.setInt(5, (int) nuevo.getTarjetaProfesional());
            sentence.setString(6, nuevo.getDireccion());
            sentence.setInt(7, (int) nuevo.getNumeroTelefono());
            sentence.setString(8, nuevo.getProfesion().toString());
            FileInputStream fis = new FileInputStream(nuevo.getTituloProfesional());
            sentence.setBlob(9, fis);
            sentence.executeUpdate();
            conexion.close();
            return true;
        } catch (Exception e) {
            System.err.println("No se pudo agregar");
            return false;
        }
    }

    public static ArrayList<Profesional> read() {
        MysqlDb m = new MysqlDb();
        Connection conexion = m.getConnection();
        try {
            ArrayList<Profesional> profesionales = new ArrayList();
            String sql = "SELECT * FROM DatosPersonales";
            Statement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery(sql);
            while (resultado.next()) {
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellidos");
                int edad = resultado.getInt("edad");
                long cedula = resultado.getInt("cedula");
                long tarjeta = resultado.getInt("tarjetaProfesional");
                String direcion = resultado.getString("direccion");
                long numero = resultado.getInt("numeroTelefono");
                TipoProfesion p = getProfesion(resultado.getString("TipoProfesional"));
                Blob blob = resultado.getBlob("titulo");
                InputStream is = blob.getBinaryStream();
                castearImagen(is, nombre);
                Profesional pro = new Profesional(nombre, apellido, edad, cedula, tarjeta, null, direcion, numero);
                pro.setProfesion(p);
                profesionales.add(pro);
            }
            conexion.close();;
            return profesionales;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("No se pudo agregar");
            return null;
        }
    }

    public static void castearImagen(InputStream is, String nombre) throws FileNotFoundException, IOException {
        File fichero = new File("src\\Imagenes\\titulo-" + nombre + ".jpg");
        if (!fichero.exists()) {
            BufferedInputStream in = new BufferedInputStream(is);
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fichero));
            byte[] img = new byte[8096];
            int len = 0;
            while ((len = in.read(img)) > 0) {
                out.write(img, 0, len);
            }

            out.flush();
            out.close();
            in.close();
        }
    }

    public static TipoProfesion getProfesion(String msg) {
        TipoProfesion[] tipos = TipoProfesion.values();
        for (TipoProfesion tp : tipos) {
            if (tp.toString().equals(msg)) {
                return tp;
            }
        }
        return null;
    }
    
    public static boolean eliminar(Profesional p){
        MysqlDb m = new MysqlDb();
        Connection conexion = m.getConnection();
        try{
            String sql = "DELETE FROM DatosPersonales WHERE cedula = '"+p.getCedula()+"'";
            Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(sql);
            conexion.close();;
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
