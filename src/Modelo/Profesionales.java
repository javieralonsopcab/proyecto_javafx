/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.TiposProfesionales.Profesional;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author javier
 */
public class Profesionales {
    
    private ArrayList<Profesional> profesionales;
    
    public Profesionales(){
        this.profesionales = new ArrayList();
    }
    
    public void insertarProfesionales(ArrayList<Profesional> profesionales){
        Iterator<Profesional> it = profesionales.iterator();
        while(it.hasNext()){
            Profesional p = it.next();
            this.profesionales.add(p);
        }
    }

    public ArrayList<Profesional> getProfesionales() {
        return profesionales;
    }

    public void setProfesionales(ArrayList<Profesional> profesionales) {
        this.profesionales = profesionales;
    }
    
    
    
}
