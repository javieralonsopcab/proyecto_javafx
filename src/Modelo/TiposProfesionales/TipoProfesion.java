/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.TiposProfesionales;

/**
 *
 * @author javier
 */
public enum TipoProfesion {
    
    Arquitecto,
    Médico,
    Enfermero,
    Veterinario,
    Contador,
    Abogado,
    Ingeniero_De_Sistemas,
    Ingeniero_Eléctronico,
    Mecánico,
    Profesor
    
    
}
