/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.TiposProfesionales;

import java.io.File;

/**
 *
 * @author javier
 */
public class Profesional implements Comparable{
    
    private String nombre;
    private String apellidos;
    private int edad;
    private long cedula;
    private long tarjetaProfesional;
    private File tituloProfesional;
    private String direccion;
    private long numeroTelefono;
    private TipoProfesion profesion;

    public Profesional(String nombre, String apellidos, int edad, long cedula, long tarjetaProfesional, File tituloProfesional, String direccion, long numeroTelefono) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.cedula = cedula;
        this.tarjetaProfesional = tarjetaProfesional;
        this.tituloProfesional = tituloProfesional;
        this.direccion = direccion;
        this.numeroTelefono = numeroTelefono;
    }
   
    
    @Override
    public boolean equals(Object other){
        Profesional p = (Profesional)other;
        return this.nombre.equals(p.nombre) && this.getApellidos().equals(p.getApellidos()) && this.getCedula() == p.getCedula() &&
                this.getTarjetaProfesional() == p.getTarjetaProfesional();
    }
    
    public String toString(){
        return "Nombre: "+this.nombre+"\nApellidos: "+this.getApellidos()+"\nEdad: "+this.edad+"\nCedula: "+this.cedula+"\nTarjetaProfesional: "+this.tarjetaProfesional;
    }

    @Override
    public int compareTo(Object t) {
       Profesional pr = (Profesional)t;
       return (int)(this.cedula - pr.cedula);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public long getTarjetaProfesional() {
        return tarjetaProfesional;
    }

    public void setTarjetaProfesional(long tarjetaProfesional) {
        this.tarjetaProfesional = tarjetaProfesional;
    }

    public File getTituloProfesional() {
        return tituloProfesional;
    }

    public void setTituloProfesional(File tituloProfesional) {
        this.tituloProfesional = tituloProfesional;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public long getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(long numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public TipoProfesion getProfesion() {
        return profesion;
    }

    public void setProfesion(TipoProfesion profesion) {
        this.profesion = profesion;
    }
}
