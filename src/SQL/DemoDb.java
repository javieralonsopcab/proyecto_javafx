/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Matias
 */
public class DemoDb {
    public static void main(String []args){
    MysqlDb m= new MysqlDb();
    Connection c = m.getConnection();
    
    try{
        PreparedStatement sentence = c.prepareStatement("INSERT INTO algo (name) VALUES(?)");
        sentence.setString(1, "Hola");
        sentence.executeUpdate();
        c.close();;
    }catch(Exception e){
        System.err.println(e.getMessage());
    }
            }
}